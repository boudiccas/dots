This is a repository for all my .dot files, bash scripts, and other files too,
which could be useful for other folk, plus it is a secure offline place to store
them too.

Further information and documentation about these files can be obtained at my
blog  <http://www.sharons.org.uk> "A Taste of Linux"

**bash** = various bash scripts  
**beeb** = the 'beeb' programme, its configuration file, and its beeb-readme.txt  
**emacs** = various emacs files  
**obnam** = files relating to my backup system, obnam  
**programmes** = programmes that I'm hosting here in case they go missing from
the internet

Sharon Kimble.  
29 January 2014.
