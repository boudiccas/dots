My various obnam files.

**'notify'v2.2** = my 'push' file, called from $USERS cron, that initiates the backup of /home  
**'obnam.conf'** = my $USER configuration file  
**'baksize'** = my script to show the size of my backup directory sizes in 'conky'  
**'unlock'v2.1** = my script to delete any 'lock' file. This is not official, just my fudge-around.

Sharon Kimble.  
November 2013.
